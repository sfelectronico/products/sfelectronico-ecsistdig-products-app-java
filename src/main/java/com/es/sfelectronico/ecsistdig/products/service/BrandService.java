package com.es.sfelectronico.ecsistdig.products.service;

import java.util.List;

import com.es.sfelectronico.ecsistdig.products.dto.BrandDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;

public interface BrandService {
	
	List<BrandDTO> findAllBrand();
	BrandDTO createBrand(BrandDTO brandDTO);
	BrandDTO editBrand(BrandDTO brandDTO);
	BrandDTO findByIdBrand(Long id) throws NotResultFound;
	void deleteBrand (Long id);
	BrandDTO changeStateBrand(Long id, String userModified) throws NotResultFound;
	
}

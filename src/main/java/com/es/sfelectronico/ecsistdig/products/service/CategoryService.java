package com.es.sfelectronico.ecsistdig.products.service;

import java.util.List;

import com.es.sfelectronico.ecsistdig.products.dto.CategoryDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;

public interface CategoryService {
	
	List<CategoryDTO> findAllCategory();
	CategoryDTO createCategory(CategoryDTO categoryDTO);
	CategoryDTO editCategory(CategoryDTO categoryDTO);
	CategoryDTO changeStateCategory(Long id, String userModified) throws NotResultFound;
	CategoryDTO findByIdCategory(Long id) throws NotResultFound;
	void deleteCategory(Long id);

}

package com.es.sfelectronico.ecsistdig.products.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_BRAND")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Brand extends BaseInfomation implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_BRAND")
	private Long idBrand;
	
	@Column(name = "COD_BRAND")
	private String codBrand;
	
	@Column(name = "DES_BRAND")
	private String desBrand;
	
	private static final long serialVersionUID = 5606556715635333908L;

}

package com.es.sfelectronico.ecsistdig.products.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.es.sfelectronico.ecsistdig.products.domain.Brand;
import com.es.sfelectronico.ecsistdig.products.dto.BrandDTO;

@Mapper
public interface BrandMapper {
	
	BrandMapper INSTANCE = Mappers.getMapper(BrandMapper.class);
	
	List<BrandDTO> convertListBrandToListBrandDTO(List<Brand> listBrand) ;
	BrandDTO convertBrandToBrandDTO(Brand brand);
	Brand convertBrandDTOToBrand(BrandDTO brandDTO);

}

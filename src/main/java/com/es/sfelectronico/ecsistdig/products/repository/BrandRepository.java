package com.es.sfelectronico.ecsistdig.products.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.sfelectronico.ecsistdig.products.domain.Brand;

public interface BrandRepository extends JpaRepository<Brand, Long> {

}

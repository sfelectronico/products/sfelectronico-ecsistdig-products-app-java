package com.es.sfelectronico.ecsistdig.products.exception;

public class NotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 7401694834222602480L;

	public NotFoundException() {}
	
	public NotFoundException(String message) {
		super(message);
	}
	
}

package com.es.sfelectronico.ecsistdig.products.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.es.sfelectronico.ecsistdig.products.dto.BrandDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;
import com.es.sfelectronico.ecsistdig.products.presentation.dto.BrandChangeStatusRequestDTO;
import com.es.sfelectronico.ecsistdig.products.service.BrandService;

@RestController
@RequestMapping("/brand")
@Validated
public class BrandController {

	private BrandService brandService;
	
	public BrandController(BrandService brandService) {
		this.brandService = brandService;
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/listAll" )
	public ResponseEntity<List<BrandDTO>>  findAllBrand() throws Exception{
		List<BrandDTO> brandFindAll = brandService.findAllBrand();
		return new ResponseEntity<>(brandFindAll, HttpStatus.OK);
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/listId/{id}")
	public ResponseEntity<BrandDTO> brandFindById(@PathVariable Long id) throws NotResultFound {
		BrandDTO brandFindId = brandService.findByIdBrand(id);
		return new ResponseEntity<>(brandFindId, HttpStatus.OK);
	}
	
	@PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/create")
	public ResponseEntity<BrandDTO> brandCreate(@Valid @RequestBody BrandDTO brandDTO) {
		BrandDTO brandCreate = brandService.createBrand(brandDTO);
		return new ResponseEntity<>(brandCreate, HttpStatus.ACCEPTED);
	}
	
	@PutMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/update")
	public ResponseEntity<BrandDTO> brandEdit(@Valid @RequestBody BrandDTO brandDTO) {
		BrandDTO brandEdit = brandService.editBrand(brandDTO);
		return new ResponseEntity<>(brandEdit, HttpStatus.ACCEPTED);
	}
	
	@PutMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/updateState")
	public ResponseEntity<BrandDTO> brandEditState(@Valid BrandChangeStatusRequestDTO brandChangeStatusRequestDTO) throws NotResultFound {
		BrandDTO brandEditState = brandService.changeStateBrand(brandChangeStatusRequestDTO.getId(), brandChangeStatusRequestDTO.getUserModified());
		return new ResponseEntity<>(brandEditState, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/delete/{id}")
	public void brandDelete(@PathVariable Long id) {
		brandService.deleteBrand(id);
	}
	
}

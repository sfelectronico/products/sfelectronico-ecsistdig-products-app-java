package com.es.sfelectronico.ecsistdig.products.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.es.sfelectronico.ecsistdig.products.dto.CategoryDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;
import com.es.sfelectronico.ecsistdig.products.presentation.dto.CategoryChangeStatusDTO;
import com.es.sfelectronico.ecsistdig.products.service.CategoryService;

@RestController
@RequestMapping("/category")
@Validated
public class CategoryController {
	
	private CategoryService categoryService;
	
	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/listAll" )
	public ResponseEntity<List<CategoryDTO>> categoryFindAll() {
		List<CategoryDTO> listCategory = categoryService.findAllCategory();
		return new ResponseEntity<>(listCategory, HttpStatus.OK);
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/listId/{id}")
	public ResponseEntity<CategoryDTO> categoryFindId(@PathVariable Long id) throws NotResultFound{
		CategoryDTO categoryFindId = categoryService.findByIdCategory(id);
		return new ResponseEntity<>(categoryFindId, HttpStatus.OK);
	}
	
	@PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/create")
	public ResponseEntity<CategoryDTO> categoryCreate(@Valid @RequestBody CategoryDTO categoryDTO){
		CategoryDTO categoryCreate = categoryService.createCategory(categoryDTO);
		return new ResponseEntity<>(categoryCreate, HttpStatus.ACCEPTED);
	}
	
	@PutMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/update")
	public ResponseEntity<CategoryDTO> categoryEdit(@RequestBody CategoryDTO categoryDTO){
		CategoryDTO categoryEdit = categoryService.editCategory(categoryDTO);
		return new ResponseEntity<>(categoryEdit, HttpStatus.ACCEPTED);
	}
	
	@PutMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/updateState")
	public ResponseEntity<CategoryDTO> categoryEditState(@Valid CategoryChangeStatusDTO categoryChangeStatusDTO) throws NotResultFound{
		CategoryDTO categoryEditStatus = categoryService.changeStateCategory(categoryChangeStatusDTO.getId(), categoryChangeStatusDTO.getUserModified());
		return new ResponseEntity<>(categoryEditStatus, HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/delete/{id}")
	public void categoryDelete(@PathVariable Long id) {
		categoryService.deleteCategory(id);
	}

}

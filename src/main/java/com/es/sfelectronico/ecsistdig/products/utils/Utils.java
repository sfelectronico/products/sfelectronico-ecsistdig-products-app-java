package com.es.sfelectronico.ecsistdig.products.utils;

public class Utils {
	
	private Utils() {
		throw new IllegalStateException("Error Utils");
	}
	
	public static String changeState(String state) {
		return (state.equals("A")) ? "D" : "A";
	}

}

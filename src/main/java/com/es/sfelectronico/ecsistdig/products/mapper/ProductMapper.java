package com.es.sfelectronico.ecsistdig.products.mapper;

import java.util.List;

import com.es.sfelectronico.ecsistdig.products.domain.Product;
import com.es.sfelectronico.ecsistdig.products.dto.ProductDTO;

public interface ProductMapper {
	
	List<ProductDTO> convertListProductToListProductDTO(List<Product> listProduct);
	ProductDTO convertProductToProductDTO(Product product);
	Product convertProductDTOToProduct(ProductDTO  productDTO);

}

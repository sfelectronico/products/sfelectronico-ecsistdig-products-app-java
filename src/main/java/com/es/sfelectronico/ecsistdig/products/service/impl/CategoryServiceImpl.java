package com.es.sfelectronico.ecsistdig.products.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.es.sfelectronico.ecsistdig.products.domain.Category;
import com.es.sfelectronico.ecsistdig.products.dto.CategoryDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;
import com.es.sfelectronico.ecsistdig.products.mapper.CategoryMapper;
import com.es.sfelectronico.ecsistdig.products.repository.CategoryRepository;
import com.es.sfelectronico.ecsistdig.products.service.CategoryService;
import com.es.sfelectronico.ecsistdig.products.utils.Constants;
import com.es.sfelectronico.ecsistdig.products.utils.Utils;

@Service
public class CategoryServiceImpl implements CategoryService {
	
	private CategoryRepository categoryRepository;
	private CategoryMapper categoryMapper;
	
	public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
		this.categoryRepository = categoryRepository;
		this.categoryMapper = categoryMapper;
	}

	@Override
	public List<CategoryDTO> findAllCategory() {
		List<Category> listCategory = categoryRepository.findAll();
		return categoryMapper.convertListCategoryToListCategoryDTO(listCategory);
	}

	@Override
	public CategoryDTO createCategory(CategoryDTO categoryDTO) {
		Category categoryCreate = categoryRepository.save(categoryMapper.convertCategoryDTOToCategory(categoryDTO));
		return categoryMapper.convertCategoryToCategoryDTO(categoryCreate);
	}

	@Override
	public CategoryDTO editCategory(CategoryDTO categoryDTO) {
		Category categoryEdit = categoryRepository.save(categoryMapper.convertCategoryDTOToCategory(categoryDTO));
		return categoryMapper.convertCategoryToCategoryDTO(categoryEdit);
	}

	@Override
	public CategoryDTO changeStateCategory(Long id, String userModified) throws NotResultFound {
		Category categoryFind = categoryRepository.findById(id)
				.orElseThrow(() -> new NotResultFound(Constants.ELEMENTO_NO_ENCONTRADO));
		categoryFind.setState(Utils.changeState(categoryFind.getState()));
		categoryFind.setUserModified(userModified);
		Category categoryChangeState = categoryRepository.save(categoryFind);
		return categoryMapper.convertCategoryToCategoryDTO(categoryChangeState);
	}

	@Override
	public CategoryDTO findByIdCategory(Long id) throws NotResultFound {
		Category category = categoryRepository.findById(id)
				.orElseThrow(() -> new NotResultFound(Constants.ELEMENTO_NO_ENCONTRADO));
		return categoryMapper.convertCategoryToCategoryDTO(category);
	}

	@Override
	public void deleteCategory(Long id) {
		categoryRepository.deleteById(id);
	}

}

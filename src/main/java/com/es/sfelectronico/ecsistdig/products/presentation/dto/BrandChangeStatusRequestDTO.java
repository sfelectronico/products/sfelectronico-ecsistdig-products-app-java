package com.es.sfelectronico.ecsistdig.products.presentation.dto;

import lombok.Data;

@Data
public class BrandChangeStatusRequestDTO {
	
	private Long id;
	private String userModified;

}

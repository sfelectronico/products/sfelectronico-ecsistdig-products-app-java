package com.es.sfelectronico.ecsistdig.products.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class BaseInfomation implements Serializable {


	@Column(name = "STATE", nullable = false, columnDefinition = "char(1) default 'A'")
	private String state;
	
	@Column(name = "USER_MODIFIED", nullable = false)
	private String userModified;
	
	@Column(name = "DATE_MODIFIED", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
	private Date dateMidified;
	
	@Column(name = "USER_CREATION", nullable = false)
	private String userCreation;
	
	@Column(name = "DATE_CREATION", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP")
	private Date dateCreation;
	
	@PrePersist
	private void postConstructor() {
		this.state = "A";
		this.dateCreation = new Date();
		this.dateMidified = new Date();
	}
	
	private static final long serialVersionUID = 1291752441778414168L;
	
}

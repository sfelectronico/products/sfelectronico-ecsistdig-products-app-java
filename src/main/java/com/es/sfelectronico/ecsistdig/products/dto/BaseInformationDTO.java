package com.es.sfelectronico.ecsistdig.products.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseInformationDTO {
	
	private String state;
	private String userModified;
	private Date dateMidified;
	private String userCreation;
	private Date dateCreation;
	
}

package com.es.sfelectronico.ecsistdig.products.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.sfelectronico.ecsistdig.products.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}

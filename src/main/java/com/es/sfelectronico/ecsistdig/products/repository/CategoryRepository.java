package com.es.sfelectronico.ecsistdig.products.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.sfelectronico.ecsistdig.products.domain.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}

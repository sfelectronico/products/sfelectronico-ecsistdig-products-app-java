package com.es.sfelectronico.ecsistdig.products.service;

import java.util.List;

import com.es.sfelectronico.ecsistdig.products.dto.ProductDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;

public interface ProductService {
	
	List<ProductDTO> findAllProduct();
	ProductDTO createProduct(ProductDTO productDTO);
	ProductDTO editProduct(ProductDTO productDTO);
	ProductDTO findByIdProduct(Long id) throws NotResultFound; 
	ProductDTO changeStateProduct(Long id, String userModified) throws NotResultFound;
	void deleteProduct(Long id);

}

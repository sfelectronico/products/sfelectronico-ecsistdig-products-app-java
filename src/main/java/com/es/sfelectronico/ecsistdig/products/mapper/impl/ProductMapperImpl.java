package com.es.sfelectronico.ecsistdig.products.mapper.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.es.sfelectronico.ecsistdig.products.domain.Product;
import com.es.sfelectronico.ecsistdig.products.dto.ProductDTO;
import com.es.sfelectronico.ecsistdig.products.mapper.ProductMapper;

@Service
public class ProductMapperImpl implements ProductMapper {
	
	private static final ModelMapper modelMapper = new ModelMapper();

	@Override
	public List<ProductDTO> convertListProductToListProductDTO(List<Product> listProduct) {
		List<ProductDTO> listProductDTO = StreamSupport.stream(listProduct.spliterator(), false)
				.map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
		return listProductDTO;
	}

	@Override
	public ProductDTO convertProductToProductDTO(Product product) {
		return modelMapper.map(product, ProductDTO.class);
	}

	@Override
	public Product convertProductDTOToProduct(ProductDTO productDTO) {
		return modelMapper.map(productDTO, Product.class);
	}

}

package com.es.sfelectronico.ecsistdig.products.mapper.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.es.sfelectronico.ecsistdig.products.domain.Category;
import com.es.sfelectronico.ecsistdig.products.dto.CategoryDTO;
import com.es.sfelectronico.ecsistdig.products.mapper.CategoryMapper;

@Service
public class CategoryMapperImpl implements CategoryMapper {
	
	private static final ModelMapper modelMapper = new ModelMapper();

	@Override
	public List<CategoryDTO> convertListCategoryToListCategoryDTO(List<Category> listCategory) {
		List<CategoryDTO> listCategoryDTO = StreamSupport.stream(listCategory.spliterator(), false)
				.map(category -> modelMapper.map(category, CategoryDTO.class)).collect(Collectors.toList());
 		return listCategoryDTO;
	}

	@Override
	public CategoryDTO convertCategoryToCategoryDTO(Category category) {
		return modelMapper.map(category, CategoryDTO.class);
	}

	@Override
	public Category convertCategoryDTOToCategory(CategoryDTO categoryDTO) {
		return modelMapper.map(categoryDTO, Category.class);
	}

}

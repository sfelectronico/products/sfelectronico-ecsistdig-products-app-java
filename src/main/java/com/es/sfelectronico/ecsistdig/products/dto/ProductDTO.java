package com.es.sfelectronico.ecsistdig.products.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDTO extends BaseInformationDTO {

	private Long idProduct;
	private String codProduct;
	private String desProduct;
	private Double stock;
	private Double price;

}

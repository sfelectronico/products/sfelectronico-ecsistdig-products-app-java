package com.es.sfelectronico.ecsistdig.products.exception;

public class BadRequestException extends RuntimeException {
	
	private static final long serialVersionUID = 943361986655360491L;

	public BadRequestException() {}

	public BadRequestException(String message) {
		super(message);
	}
}

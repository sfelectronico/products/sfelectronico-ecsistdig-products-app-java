package com.es.sfelectronico.ecsistdig.products.presentation.dto;

import lombok.Data;

@Data
public class CategoryChangeStatusDTO {

	private Long id;
	private String userModified;

}

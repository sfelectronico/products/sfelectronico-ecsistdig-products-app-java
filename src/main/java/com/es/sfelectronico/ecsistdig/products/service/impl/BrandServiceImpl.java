package com.es.sfelectronico.ecsistdig.products.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.es.sfelectronico.ecsistdig.products.domain.Brand;
import com.es.sfelectronico.ecsistdig.products.dto.BrandDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;
import com.es.sfelectronico.ecsistdig.products.mapper.BrandMapper;
import com.es.sfelectronico.ecsistdig.products.repository.BrandRepository;
import com.es.sfelectronico.ecsistdig.products.service.BrandService;
import com.es.sfelectronico.ecsistdig.products.utils.Constants;
import com.es.sfelectronico.ecsistdig.products.utils.Utils;

@Service
public class BrandServiceImpl implements BrandService {

	private BrandRepository brandRepository;
	private BrandMapper brandMapper;

	public BrandServiceImpl(BrandRepository brandRepository, BrandMapper brandMapper) {
		this.brandRepository = brandRepository;
		this.brandMapper = brandMapper;
	}

	@Override
	@Transactional(readOnly = true)
	public List<BrandDTO> findAllBrand() {
		List<Brand> listBrand = brandRepository.findAll();
		return listBrand.stream().map(brandMapper::convertBrandToBrandDTO).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = false)
	public BrandDTO createBrand(BrandDTO brandDTO) {
		Brand brand = brandRepository.save(brandMapper.convertBrandDTOToBrand(brandDTO));
		return brandMapper.convertBrandToBrandDTO(brand);
	}

	@Override
	@Transactional(readOnly = false)
	public BrandDTO editBrand(BrandDTO brandDTO) {
		Brand brandEdit = brandRepository.save(brandMapper.convertBrandDTOToBrand(brandDTO));
		return brandMapper.convertBrandToBrandDTO(brandEdit);
	}

	@Override
	@Transactional(readOnly = false)
	public BrandDTO changeStateBrand(Long id, String userModified) throws NotResultFound {

		Brand brandFind = brandRepository.findById(id)
				.orElseThrow(() -> new NotResultFound(Constants.ELEMENTO_NO_ENCONTRADO));
		brandFind.setState(Utils.changeState(brandFind.getState()));
		brandFind.setDateMidified(new Date());
		brandFind.setUserModified(userModified);
		Brand brandChangeState = brandRepository.save(brandFind);

		return brandMapper.convertBrandToBrandDTO(brandChangeState);
	}

	@Override
	@Transactional(readOnly = true)
	public BrandDTO findByIdBrand(Long id) throws NotResultFound {
		Brand brand = brandRepository.findById(id)
				.orElseThrow(() -> new NotResultFound(Constants.ELEMENTO_NO_ENCONTRADO));
		return brandMapper.convertBrandToBrandDTO(brand);
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteBrand(Long id) {
		brandRepository.deleteById(id);
	}

}

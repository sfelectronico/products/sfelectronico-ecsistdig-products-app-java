package com.es.sfelectronico.ecsistdig.products.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_CATEGORY")
@Getter
@Setter
public class Category extends BaseInfomation implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CATEGORY")
	private Long idCategory;
	
	@Column(name = "COD_CATEGORY")
	private String codCategory;
	
	@Column(name = "DES_CATEGORY")
	private String desCategory;
	
	private static final long serialVersionUID = 6000060933536763349L;
	
}

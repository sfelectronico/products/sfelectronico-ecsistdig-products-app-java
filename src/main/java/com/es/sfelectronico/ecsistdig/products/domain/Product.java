package com.es.sfelectronico.ecsistdig.products.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_PRODUCT")
@Getter
@Setter
public class Product extends BaseInfomation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_PRODUCT")
	private Long idProduct;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_BRAND", referencedColumnName = "ID_BRAND")
	private Brand brand;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CATEGORY", referencedColumnName = "ID_CATEGORY")
	private Category category;
	
	@Column(name = "COD_PRODUCT")
	private String codProduct;
	
	@Column(name = "DES_PRODUCT")
	private String desProduct;
	
	@Column(name = "STOCK")
	private Double stock;
	
	@Column(name = "PRICE")
	private Double price;
	
	private static final long serialVersionUID = -6164923582737169893L;

}

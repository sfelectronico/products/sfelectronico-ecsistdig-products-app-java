package com.es.sfelectronico.ecsistdig.products.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.es.sfelectronico.ecsistdig.products.dto.ProductDTO;
import com.es.sfelectronico.ecsistdig.products.service.ProductService;

@RequestMapping("/product")
@RestController
@Validated
public class ProductController {
	
	private ProductService productService;
	
	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/listAll" )
	public ResponseEntity<List<ProductDTO>> productFindAll() {
		List<ProductDTO> listProduct = productService.findAllProduct();
		return new ResponseEntity<>(listProduct, HttpStatus.OK);
	}

}

package com.es.sfelectronico.ecsistdig.products.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryDTO extends BaseInformationDTO {

	private Long idCategory;
	private String codCategory;
	private String desCategory;

}

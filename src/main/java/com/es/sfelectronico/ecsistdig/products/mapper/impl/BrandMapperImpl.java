package com.es.sfelectronico.ecsistdig.products.mapper.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.es.sfelectronico.ecsistdig.products.domain.Brand;
import com.es.sfelectronico.ecsistdig.products.dto.BrandDTO;
import com.es.sfelectronico.ecsistdig.products.mapper.BrandMapper;

@Service
public class BrandMapperImpl implements BrandMapper {
	
	private static final ModelMapper modelMapper = new ModelMapper();

	@Override
	public List<BrandDTO> convertListBrandToListBrandDTO(List<Brand> listBrand) {
		List<BrandDTO> listBrandDTO = StreamSupport.stream(listBrand.spliterator(), false)
				.map(brand -> modelMapper.map(brand, BrandDTO.class)).collect(Collectors.toList());
		return listBrandDTO;
	}

	@Override
	public BrandDTO convertBrandToBrandDTO(Brand brand) {
		return modelMapper.map(brand, BrandDTO.class);
	}

	@Override
	public Brand convertBrandDTOToBrand(BrandDTO brandDTO) {
		return modelMapper.map(brandDTO, Brand.class);
	}

}

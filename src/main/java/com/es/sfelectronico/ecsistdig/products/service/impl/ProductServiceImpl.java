package com.es.sfelectronico.ecsistdig.products.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.es.sfelectronico.ecsistdig.products.domain.Product;
import com.es.sfelectronico.ecsistdig.products.dto.ProductDTO;
import com.es.sfelectronico.ecsistdig.products.exception.NotResultFound;
import com.es.sfelectronico.ecsistdig.products.mapper.ProductMapper;
import com.es.sfelectronico.ecsistdig.products.repository.ProductRepository;
import com.es.sfelectronico.ecsistdig.products.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	private ProductMapper productMapper;
	private ProductRepository productRepository;
	
	public ProductServiceImpl(ProductMapper productMapper, ProductRepository productRepository) {
		this.productMapper = productMapper;
		this.productRepository = productRepository;
	}

	@Override
	public List<ProductDTO> findAllProduct() {
		List<Product> listProduct = productRepository.findAll();
		return productMapper.convertListProductToListProductDTO(listProduct);
	}

	@Override
	public ProductDTO createProduct(ProductDTO productDTO) {
		Product product = productRepository.save(productMapper.convertProductDTOToProduct(productDTO));
		return productMapper.convertProductToProductDTO(product);
	}

	@Override
	public ProductDTO editProduct(ProductDTO productDTO) {
		
		return null;
	}

	@Override
	public ProductDTO findByIdProduct(Long id) throws NotResultFound {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductDTO changeStateProduct(Long id, String userModified) throws NotResultFound {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteProduct(Long id) {
		// TODO Auto-generated method stub
		
	}

}

package com.es.sfelectronico.ecsistdig.products.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NotResultFound extends Exception {
	
	private static final long serialVersionUID = 9164480999553257719L;
	private String message;

}

package com.es.sfelectronico.ecsistdig.products.mapper;

import java.util.List;

import com.es.sfelectronico.ecsistdig.products.domain.Category;
import com.es.sfelectronico.ecsistdig.products.dto.CategoryDTO;

public interface CategoryMapper {
	
	List<CategoryDTO> convertListCategoryToListCategoryDTO(List<Category> category);
	CategoryDTO convertCategoryToCategoryDTO(Category category);
	Category convertCategoryDTOToCategory(CategoryDTO categoryDTO);

}

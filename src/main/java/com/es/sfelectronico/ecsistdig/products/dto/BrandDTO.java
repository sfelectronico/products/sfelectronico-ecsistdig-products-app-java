package com.es.sfelectronico.ecsistdig.products.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrandDTO extends BaseInformationDTO {
	
	private Long idBrand;
	private String codBrand;
	private String desBrand;

}
